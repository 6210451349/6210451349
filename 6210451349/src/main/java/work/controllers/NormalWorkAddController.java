package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import work.models.NormalWork;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class NormalWorkAddController {

    @FXML
    private TextField workNormalAddBackName;
    @FXML
    private DatePicker workNormalAddBackDate;
    @FXML
    private TextField workNormalAddBackStart;
    @FXML
    private TextField workNormalAddBackEnd;
    @FXML
    private ComboBox workNormalAddIndex;
    @FXML
    private ComboBox workNormalAddStatus;

    NormalWork dataEdit = (NormalWork) FXRouter.getData();

    @FXML
    public void initialize() {
        System.out.println("initialize NormalWorkAddController");
        if (dataEdit != null) {
            workNormalAddBackName.setText(dataEdit.getNameWork());
            workNormalAddBackDate.setValue(dataEdit.getDateWork());
            workNormalAddBackStart.setText(dataEdit.getStartWork());
            workNormalAddBackEnd.setText(dataEdit.getEndWork());
            workNormalAddIndex.setValue(dataEdit.getOrder());
            workNormalAddStatus.setValue(dataEdit.getStatus());
        }
        ObservableList<String> list = FXCollections.observableArrayList("ปกติ", "ด่วน", "ด่วนมาก");
        workNormalAddIndex.setItems(list);
        ObservableList<String> list2 = FXCollections.observableArrayList("ยังไม่เริ่ม", "กำลังทำ", "เสร็จสิ้น");
        workNormalAddStatus.setItems(list2);
        workNormalAddBackDate.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }

    @FXML
    public void backNormalWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("normal_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า normal_work ไม่ได้");
        }
    }

    @FXML
    public void btnWorkNormalAddSave(ActionEvent actionEvent) throws IOException {
        String name = workNormalAddBackName.getText();
        LocalDate date = workNormalAddBackDate.getValue();
        String start = workNormalAddBackStart.getText();
        String end = workNormalAddBackEnd.getText();
        String index = workNormalAddIndex.getSelectionModel().getSelectedItem().toString();
        String status = workNormalAddStatus.getSelectionModel().getSelectedItem().toString();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        // count csv
        String line = "";
        String splitBy = ",";
        ObservableList<NormalWork> listNormal = FXCollections.observableArrayList();
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("normalWork.csv"));
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] row = line.split(splitBy);    // use comma as separator
                if (dataEdit != null && dataEdit.getID() == count) {
                    listNormal.add(new NormalWork(count, name, LocalDate.parse(dateFormatter.format(date)), start, end, index, status));
                    System.out.println("change");
                } else {
                    listNormal.add(new NormalWork(count, row[1], LocalDate.parse(row[2], dateFormatter), row[3], row[4], row[5], row[6]));
                    System.out.println(listNormal.get(count).getID());
                }
                count++;
            }
            if (count == 0 || dataEdit == null) {
                listNormal.add(new NormalWork(count, name, LocalDate.parse(dateFormatter.format(date)), start, end, index, status));
                System.out.println("change");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//         save csv
        Writer writer = null;
        try {
            File file = new File("normalWork.csv");
            writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < count; i++) {
                writer.write(listNormal.get(i).getID() + "," +
                        listNormal.get(i).getNameWork() + "," +
                        listNormal.get(i).getDateWork() + "," +
                        listNormal.get(i).getStartWork() + "," +
                        listNormal.get(i).getEndWork() + "," +
                        listNormal.get(i).getOrder() + "," +
                        listNormal.get(i).getStatus() + "\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
        try {
            FXRouter.goTo("normal_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า normal_work ไม่ได้");
        }
    }


}
