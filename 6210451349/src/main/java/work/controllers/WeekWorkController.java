package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import work.models.WeekWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WeekWorkController {

    @FXML
    private TableView<WeekWork> table_nameWeek;
    @FXML
    private TableColumn<WeekWork, String> nameWork;
    @FXML
    private TableColumn<WeekWork, String> startDateWork;
    @FXML
    private TableColumn<WeekWork, String> endDateWork;
    @FXML
    private TableColumn<WeekWork, String> startWork;
    @FXML
    private TableColumn<WeekWork, String> endWork;
    @FXML
    private TableColumn<WeekWork, String> order;
    @FXML
    private TableColumn<WeekWork, String> status;

    ObservableList<WeekWork> listWeek = FXCollections.observableArrayList();
    int count = 0;

    @FXML
    public void initialize() {
        System.out.println("initialize WeekWorkController");
        String line = "";
        String splitBy = ",";
        try {
            BufferedReader br = new BufferedReader(new FileReader("weekWork.csv"));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(splitBy);
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                WeekWork weekWork = new WeekWork(Integer.parseInt(row[0]), row[1], LocalDate.parse(row[2], dateFormatter), LocalDate.parse(row[3], dateFormatter), row[4], row[5], row[6], row[7]);
                listWeek.add(weekWork);
                System.out.println(listWeek.get(0).getStartDate());
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        nameWork.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("nameWork"));
        startDateWork.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("startDate"));
        endDateWork.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("endDate"));
        startWork.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("startWork"));
        endWork.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("stopWork"));
        order.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("order"));
        status.setCellValueFactory(new PropertyValueFactory<WeekWork, String>("status"));
        table_nameWeek.setItems(listWeek);
    }

    @FXML
    public void editWeekWork(ActionEvent actionEvent) {
        WeekWork dataEdit = new WeekWork();
        dataEdit = table_nameWeek.getSelectionModel().getSelectedItem();
        if (dataEdit != null) {
            try {
                FXRouter.goTo("week_work_add", dataEdit);
            } catch (IOException e) {
                System.err.println("ไปที่หน้า week_work_add ไม่ได้");
            }
        }

    }

    @FXML
    public void deleteWeekWork(ActionEvent actionEvent) throws IOException {
        int row = -1;
        row = table_nameWeek.getSelectionModel().getSelectedIndex();
        System.out.println(listWeek.size());
        System.out.println(row);
        if (row != -1) {
            listWeek.remove(row);
            Writer writer = null;
            try {
                File file = new File("weekWork.csv");
                writer = new BufferedWriter(new FileWriter(file));
                System.out.println(listWeek.size());
                for (int i = 0; i < listWeek.size(); i++) {
                    writer.write(listWeek.get(i).getID() + "," +
                            listWeek.get(i).getNameWork() + "," +
                            listWeek.get(i).getStartDate() + "," +
                            listWeek.get(i).getEndDate() + "," +
                            listWeek.get(i).getStartWork() + "," +
                            listWeek.get(i).getStopWork() + "," +
                            listWeek.get(i).getOrder() + "," +
                            listWeek.get(i).getStatus() + "\n");
                }
                table_nameWeek.setItems(listWeek);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.flush();
                writer.close();
            }
        }

    }

    @FXML
    public void backWeekWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า home ไม่ได้");
        }
    }

    @FXML
    public void goToWeekWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("week_work_add", null);
        } catch (IOException e) {
            System.err.println("ไปที่หน้า week_work_add ไม่ได้");
        }
    }


}
