package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import work.models.NormalWork;
import work.models.ProjectWork;
import work.models.WeekWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ProjectWorkController {

    @FXML
    private TableView<ProjectWork> table_nameProject;
    @FXML
    private TableColumn<ProjectWork, String> nameWork;
    @FXML
    private TableColumn<ProjectWork, String> nameHead;
    @FXML
    private TableColumn<ProjectWork, String> startDateWork;
    @FXML
    private TableColumn<ProjectWork, String> endDateWork;
    @FXML
    private TableColumn<ProjectWork, String> order;
    @FXML
    private TableColumn<ProjectWork, String> status;

    ObservableList<ProjectWork> listProject = FXCollections.observableArrayList();
    int count = 0;

    @FXML
    public void initialize() {
        System.out.println("initialize ProjectWorkController");
        String line = "";
        String splitBy = ",";
        try {
            BufferedReader br = new BufferedReader(new FileReader("projectWork.csv"));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(splitBy);
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                ProjectWork projectWork = new ProjectWork(Integer.parseInt(row[0]), row[1], row[2], LocalDate.parse(row[3], dateFormatter), LocalDate.parse(row[4], dateFormatter),row[5], row[6]);
                listProject.add(projectWork);
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        nameWork.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("nameWork"));
        nameHead.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("nameHead"));
        startDateWork.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("startDate"));
        endDateWork.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("endDate"));
        order.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("order"));
        status.setCellValueFactory(new PropertyValueFactory<ProjectWork, String>("status"));
        table_nameProject.setItems(listProject);
    }

    @FXML
    public void editNormalWork(ActionEvent actionEvent) {
        ProjectWork dataEdit = new ProjectWork();
        dataEdit = table_nameProject.getSelectionModel().getSelectedItem();
        if (dataEdit != null) {
            try {
                FXRouter.goTo("project_work_add", dataEdit);
            } catch (IOException e) {
                System.err.println("ไปที่หน้า project_work_add ไม่ได้");
            }
        }

    }

    @FXML
    public void deleteNormalWork(ActionEvent actionEvent) throws IOException {
        int row = -1;
        row = table_nameProject.getSelectionModel().getSelectedIndex();
        System.out.println(listProject.size());
        System.out.println(row);
        if (row != -1) {
            listProject.remove(row);
            Writer writer = null;
            try {
                File file = new File("projectWork.csv");
                writer = new BufferedWriter(new FileWriter(file));
                System.out.println(listProject.size());
                for (int i = 0; i < listProject.size(); i++) {
                    writer.write(listProject.get(i).getID() + "," +
                            listProject.get(i).getNameWork() + "," +
                            listProject.get(i).getNameHead() + "," +
                            listProject.get(i).getStartDate() + "," +
                            listProject.get(i).getEndDate() + "," +
                            listProject.get(i).getOrder() + "," +
                            listProject.get(i).getStatus() + "\n");
                }
                table_nameProject.setItems(listProject);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.flush();
                writer.close();
            }
        }

    }

    @FXML
    public void backNormalWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า home ไม่ได้");
        }
    }

    @FXML
    public void goToNormalWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("project_work_add", null);
        } catch (IOException e) {
            System.err.println("ไปที่หน้า project_work_add ไม่ได้");
        }
    }
}
