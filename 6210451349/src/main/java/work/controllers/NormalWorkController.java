package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import work.models.NormalWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NormalWorkController {

    @FXML
    private TableView<NormalWork> table_nameWork;
    @FXML
    private TableColumn<NormalWork, String> nameWork;
    @FXML
    private TableColumn<NormalWork, String> dateWork;
    @FXML
    private TableColumn<NormalWork, String> startWork;
    @FXML
    private TableColumn<NormalWork, String> endWork;
    @FXML
    private TableColumn<NormalWork, String> order;
    @FXML
    private TableColumn<NormalWork, String> status;

    ObservableList<NormalWork> listNormal = FXCollections.observableArrayList();
    int count = 0;

    @FXML
    public void initialize() {
        System.out.println("initialize NormalWorkController");
        String line = "";
        String splitBy = ",";
        try {
            BufferedReader br = new BufferedReader(new FileReader("normalWork.csv"));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(splitBy);    // use comma as separator
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                NormalWork normalWork = new NormalWork(Integer.parseInt(row[0]), row[1], LocalDate.parse(row[2], dateFormatter), row[3], row[4], row[5], row[6]);
                listNormal.add(normalWork);
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        nameWork.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("nameWork"));
        dateWork.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("dateWork"));
        startWork.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("startWork"));
        endWork.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("endWork"));
        order.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("order"));
        status.setCellValueFactory(new PropertyValueFactory<NormalWork, String>("status"));
        table_nameWork.setItems(listNormal);
    }

    @FXML
    public void editNormalWork(ActionEvent actionEvent) {
        NormalWork dataEdit = new NormalWork();
        dataEdit = table_nameWork.getSelectionModel().getSelectedItem();
        if (dataEdit != null) {
            try {
                FXRouter.goTo("normal_work_add", dataEdit);
            } catch (IOException e) {
                System.err.println("ไปที่หน้า normal_work_add ไม่ได้");
            }
        }

    }

    @FXML
    public void deleteNormalWork(ActionEvent actionEvent) throws IOException {
        int row = -1;
        row = table_nameWork.getSelectionModel().getSelectedIndex();
        System.out.println(listNormal.size());
        System.out.println(row);
        if (row != -1) {
            listNormal.remove(row);
            Writer writer = null;
            try {
                File file = new File("normalWork.csv");
                writer = new BufferedWriter(new FileWriter(file));
                System.out.println(listNormal.size());
                for (int i = 0; i < listNormal.size(); i++) {
                    writer.write(listNormal.get(i).getID() + "," +
                            listNormal.get(i).getNameWork() + "," +
                            listNormal.get(i).getDateWork() + "," +
                            listNormal.get(i).getStartWork() + "," +
                            listNormal.get(i).getEndWork() + "," +
                            listNormal.get(i).getOrder() + "," +
                            listNormal.get(i).getStatus() + "\n");
                }
                table_nameWork.setItems(listNormal);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.flush();
                writer.close();
            }
        }

    }

    @FXML
    public void backNormalWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า home ไม่ได้");
        }
    }

    @FXML
    public void goToNormalWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("normal_work_add", null);
        } catch (IOException e) {
            System.err.println("ไปที่หน้า normal_work_add ไม่ได้");
        }
    }

}
