package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import work.models.NormalWork;
import work.models.WeekWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WeekWorkAddController {

    @FXML private TextField workWeekAddName;
    @FXML private DatePicker workWeekAddStart;
    @FXML private DatePicker workWeekAddEnd;
    @FXML private TextField workNormalAddTimeStart;
    @FXML private TextField workWeekAddTimeEnd;
    @FXML private ComboBox workWeekAddIndex;
    @FXML private ComboBox workWeekAddStatus;

    WeekWork dataEdit = (WeekWork) FXRouter.getData();

    @FXML
    public void initialize() {
        System.out.println("initialize NormalWorkAddController");
        if (dataEdit != null) {
            workWeekAddName.setText(dataEdit.getNameWork());
            workWeekAddStart.setValue(dataEdit.getStartDate());
            workWeekAddEnd.setValue(dataEdit.getEndDate());
            workNormalAddTimeStart.setText(dataEdit.getStartWork());
            workWeekAddTimeEnd.setText(dataEdit.getStopWork());
            workWeekAddIndex.setValue(dataEdit.getOrder());
            workWeekAddStatus.setValue(dataEdit.getStatus());
        }
        ObservableList<String> list = FXCollections.observableArrayList("ปกติ", "ด่วน", "ด่วนมาก");
        workWeekAddIndex.setItems(list);
        ObservableList<String> list2 = FXCollections.observableArrayList("ยังไม่เริ่ม", "กำลังทำ", "เสร็จสิ้น");
        workWeekAddStatus.setItems(list2);
        workWeekAddStart.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        workWeekAddEnd.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }

    @FXML
    public void backWeekWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("week_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า week_work ไม่ได้");
        }
    }

    @FXML
    public void btnWorkWeekAddSave(ActionEvent actionEvent) throws IOException {

        String name = workWeekAddName.getText();
        LocalDate date1 = workWeekAddStart.getValue();
        LocalDate date2 = workWeekAddEnd.getValue();
        String start = workNormalAddTimeStart.getText();
        String end = workWeekAddTimeEnd.getText();
        String index = workWeekAddIndex.getSelectionModel().getSelectedItem().toString();
        String status = workWeekAddStatus.getSelectionModel().getSelectedItem().toString();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        // count csv
        String line = "";
        String splitBy = ",";
        ObservableList<WeekWork> listWeek = FXCollections.observableArrayList();
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("weekWork.csv"));
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] row = line.split(splitBy);    // use comma as separator
                if (dataEdit != null && dataEdit.getID() == count) {
                    listWeek.add(new WeekWork(count, name, LocalDate.parse(dateFormatter.format(date1)), LocalDate.parse(dateFormatter.format(date2)),start, end, index, status));
                    System.out.println("change");
                } else {
                    listWeek.add(new WeekWork(count, row[1], LocalDate.parse(row[2], dateFormatter), LocalDate.parse(row[3], dateFormatter), row[4], row[5], row[6], row[7]));
                    System.out.println(listWeek.get(count).getID());
                }
                count++;
            }
            if (count == 0 || dataEdit == null) {
                listWeek.add(new WeekWork(count, name, LocalDate.parse(dateFormatter.format(date1)), LocalDate.parse(dateFormatter.format(date2)),start, end, index, status));
                System.out.println("change");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//         save csv
        Writer writer = null;
        try {
            File file = new File("weekWork.csv");
            writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < count; i++) {
                writer.write(listWeek.get(i).getID() + "," +
                        listWeek.get(i).getNameWork() + "," +
                        listWeek.get(i).getStartDate() + "," +
                        listWeek.get(i).getEndDate() + "," +
                        listWeek.get(i).getStartWork() + "," +
                        listWeek.get(i).getStopWork() + "," +
                        listWeek.get(i).getOrder() + "," +
                        listWeek.get(i).getStatus() + "\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
        try {
            FXRouter.goTo("week_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า week_work ไม่ได้");
        }
    }

}
