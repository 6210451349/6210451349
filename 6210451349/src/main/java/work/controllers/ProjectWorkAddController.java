package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import work.models.ProjectWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ProjectWorkAddController {

    @FXML private TextField workProjectAddName;
    @FXML private TextField workProjectAddPerson;
    @FXML private DatePicker workProjectAddStart;
    @FXML private DatePicker workProjectAddEnd;
    @FXML private ComboBox workProjectAddIndex;
    @FXML private ComboBox workProjectAddStatus;

    ProjectWork dataEdit = (ProjectWork) FXRouter.getData();

    @FXML
    public void initialize() {
        System.out.println("initialize ProjectAddController");
        if (dataEdit != null) {
            workProjectAddName.setText(dataEdit.getNameWork());
            workProjectAddPerson.setText(dataEdit.getNameHead());
            workProjectAddStart.setValue(dataEdit.getStartDate());
            workProjectAddEnd.setValue(dataEdit.getEndDate());
            workProjectAddIndex.setValue(dataEdit.getOrder());
            workProjectAddStatus.setValue(dataEdit.getStatus());
        }
        System.out.println(dataEdit);
        ObservableList<String> list = FXCollections.observableArrayList("ปกติ","ด่วน","ด่วนมาก");
        workProjectAddIndex.setItems(list);
        ObservableList<String> list2 = FXCollections.observableArrayList("ยังไม่เริ่ม","กำลังทำ","เสร็จสิ้น");
        workProjectAddStatus.setItems(list2);
        workProjectAddStart.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        workProjectAddEnd.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }

    @FXML
    public void backProjectWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("project_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า project_work ไม่ได้");
        }
    }

    @FXML
    public void btnProjectWeekAddSave(ActionEvent actionEvent) throws IOException {
        String nameWork = workProjectAddName.getText();
        String nameHead = workProjectAddPerson.getText();
        LocalDate start = workProjectAddStart.getValue();
        LocalDate end = workProjectAddEnd.getValue();
        String index = workProjectAddIndex.getSelectionModel().getSelectedItem().toString();
        String status = workProjectAddStatus.getSelectionModel().getSelectedItem().toString();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        // count csv
        String line = "";
        String splitBy = ",";
        ObservableList<ProjectWork> listProject = FXCollections.observableArrayList();
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("projectWork.csv"));
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] row = line.split(splitBy);    // use comma as separator
                if (dataEdit != null && dataEdit.getID() == count) {
                    listProject.add(new ProjectWork(count, nameWork, nameHead, LocalDate.parse(dateFormatter.format(start)), LocalDate.parse(dateFormatter.format(end)), index, status));
                    System.out.println("change");
                } else {
                    listProject.add(new ProjectWork(count, row[1], row[2], LocalDate.parse(row[3], dateFormatter), LocalDate.parse(row[4], dateFormatter), row[5], row[6]));
                    System.out.println(listProject.get(count).getID());
                }
                count++;
            }
            if (count == 0 || dataEdit == null) {
                listProject.add(new ProjectWork(count, nameWork, nameHead, LocalDate.parse(dateFormatter.format(start)), LocalDate.parse(dateFormatter.format(end)), index, status));
                System.out.println("change");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//         save csv
        Writer writer = null;
        try {
            File file = new File("projectWork.csv");
            writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < count; i++) {
                writer.write(listProject.get(i).getID() + "," +
                        listProject.get(i).getNameWork() + "," +
                        listProject.get(i).getNameHead() + "," +
                        listProject.get(i).getStartDate() + "," +
                        listProject.get(i).getEndDate() + "," +
                        listProject.get(i).getOrder() + "," +
                        listProject.get(i).getStatus() + "\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
        try {
            FXRouter.goTo("project_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า project_work ไม่ได้");
        }
    }

}
