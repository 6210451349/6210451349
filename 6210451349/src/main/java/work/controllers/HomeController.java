package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;

public class HomeController {

    @FXML
    public void initialize() {

    }

    @FXML
    public void goToNormalWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("normal_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า normal_work ไม่ได้");
        }
    }

    @FXML
    public void goToWeekWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("week_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า week_work ไม่ได้");
        }
    }

    @FXML
    public void goToConWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("con_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า con_work ไม่ได้");
        }
    }

    @FXML
    public void goToProjectWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("project_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า project_work ไม่ได้");
        }
    }

    @FXML
    public void goToAbout(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("about");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า about ไม่ได้");
        }
    }

}
