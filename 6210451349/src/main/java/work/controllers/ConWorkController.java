package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import work.models.ConWork;
import work.models.NormalWork;
import work.models.WeekWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ConWorkController {

    @FXML
    private TableView<ConWork> table_nameCon;
    @FXML
    private TableColumn<ConWork, String> nameWork;
    @FXML
    private TableColumn<ConWork, String> namePerson;
    @FXML
    private TableColumn<ConWork, String> order;
    @FXML
    private TableColumn<ConWork, String> status;

    ObservableList<ConWork> listCon = FXCollections.observableArrayList();
    int count = 0;

    @FXML
    public void initialize() {
        System.out.println("initialize ConWorkController");
        String line = "";
        String splitBy = ",";
        try {
            BufferedReader br = new BufferedReader(new FileReader("conWork.csv"));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(splitBy);
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                ConWork conWork = new ConWork(Integer.parseInt(row[0]), row[1], row[2], row[3], row[4]);
                listCon.add(conWork);
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        nameWork.setCellValueFactory(new PropertyValueFactory<ConWork, String>("nameWork"));
        namePerson.setCellValueFactory(new PropertyValueFactory<ConWork, String>("namePerson"));
        order.setCellValueFactory(new PropertyValueFactory<ConWork, String>("order"));
        status.setCellValueFactory(new PropertyValueFactory<ConWork, String>("status"));
        table_nameCon.setItems(listCon);
    }

    @FXML
    public void editNormalWork(ActionEvent actionEvent) {
        ConWork dataEdit = new ConWork();
        dataEdit = table_nameCon.getSelectionModel().getSelectedItem();
        if (dataEdit != null) {
            try {
                FXRouter.goTo("con_work_add", dataEdit);
            } catch (IOException e) {
                System.err.println("ไปที่หน้า con_work_add ไม่ได้");
            }
        }

    }

    @FXML
    public void deleteNormalWork(ActionEvent actionEvent) throws IOException {
        int row = -1;
        row = table_nameCon.getSelectionModel().getSelectedIndex();
        System.out.println(listCon.size());
        System.out.println(row);
        if (row != -1) {
            listCon.remove(row);
            Writer writer = null;
            try {
                File file = new File("conWork.csv");
                writer = new BufferedWriter(new FileWriter(file));
                System.out.println(listCon.size());
                for (int i = 0; i < listCon.size(); i++) {
                    writer.write(listCon.get(i).getID() + "," +
                            listCon.get(i).getNameWork() + "," +
                            listCon.get(i).getNamePerson() + "," +
                            listCon.get(i).getOrder() + "," +
                            listCon.get(i).getStatus() + "\n");
                }
                table_nameCon.setItems(listCon);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                writer.flush();
                writer.close();
            }
        }

    }

    @FXML
    public void backNormalWork(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า home ไม่ได้");
        }
    }

    @FXML
    public void goToNormalWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("con_work_add", null);
        } catch (IOException e) {
            System.err.println("ไปที่หน้า con_work_add ไม่ได้");
        }
    }
}
