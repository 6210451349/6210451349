package work.controllers;

import com.github.saacsos.FXRouter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import work.models.ConWork;
import work.models.NormalWork;
import work.models.WeekWork;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ConWorkAddController {

    @FXML
    private TextField workConAddName;
    @FXML
    private TextField workConAddPerson;
    @FXML
    private ComboBox workConAddIndex;
    @FXML
    private ComboBox workConAddStatus;

    ConWork dataEdit = (ConWork) FXRouter.getData();

    @FXML
    public void initialize() {
        System.out.println("initialize ConWorkAddController");
        if (dataEdit != null) {
            workConAddName.setText(dataEdit.getNameWork());
            workConAddPerson.setText(dataEdit.getNamePerson());
            workConAddIndex.setValue(dataEdit.getOrder());
            workConAddStatus.setValue(dataEdit.getStatus());
        }
        ObservableList<String> list = FXCollections.observableArrayList("ปกติ", "ด่วน", "ด่วนมาก");
        workConAddIndex.setItems(list);
        ObservableList<String> list2 = FXCollections.observableArrayList("ยังไม่เริ่ม", "กำลังทำ", "เสร็จสิ้น");
        workConAddStatus.setItems(list2);
    }

    @FXML
    public void backConWorkAdd(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("con_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า con_work ไม่ได้");
        }
    }

    @FXML
    public void btnWorkConAddSave(ActionEvent actionEvent) throws IOException {
        String name = workConAddName.getText();
        String person = workConAddPerson.getText();
        String index = workConAddIndex.getSelectionModel().getSelectedItem().toString();
        String status = workConAddStatus.getSelectionModel().getSelectedItem().toString();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        // count csv
        String line = "";
        String splitBy = ",";
        ObservableList<ConWork> listCon = FXCollections.observableArrayList();
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("conWork.csv"));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(splitBy);    // use comma as separator
                if (dataEdit != null && dataEdit.getID() == count) {
                    listCon.add(new ConWork(count, name, person, index, status));
                    System.out.println("change");
                } else {
                    listCon.add(new ConWork(count, row[1], row[2], row[3], row[4]));
                    System.out.println(listCon.get(count).getID());
                }
                count++;
            }
            if (count == 0 || dataEdit == null) {
                listCon.add(new ConWork(count, name, person, index, status));
                System.out.println("change");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//         save csv
        Writer writer = null;
        try {
            File file = new File("conWork.csv");
            writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < count; i++) {
                writer.write(listCon.get(i).getID() + "," +
                        listCon.get(i).getNameWork() + "," +
                        listCon.get(i).getNamePerson() + "," +
                        listCon.get(i).getOrder() + "," +
                        listCon.get(i).getStatus() + "\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
        try {
            FXRouter.goTo("con_work");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า con_work ไม่ได้");
        }
    }

}
