package work.models;

import java.time.LocalDate;

public class ProjectWork {

    private int ID;
    private String nameWork;
    private String nameHead;
    private LocalDate startDate;
    private LocalDate endDate;
    private String order;
    private String status;

    public ProjectWork() {
    }

    public ProjectWork(int ID, String nameWork, String nameHead, LocalDate startDate, LocalDate endDate, String order, String status) {
        this.ID = ID;
        this.nameWork = nameWork;
        this.nameHead = nameHead;
        this.startDate = startDate;
        this.endDate = endDate;
        this.order = order;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameWork() {
        return nameWork;
    }

    public void setNameWork(String nameWork) {
        this.nameWork = nameWork;
    }

    public String getNameHead() {
        return nameHead;
    }

    public void setNameHead(String nameHead) {
        this.nameHead = nameHead;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
