package work.models;

import java.time.LocalDate;

public class WeekWork {

    private int ID;
    private String nameWork;
    private LocalDate startDate;
    private LocalDate endDate;
    private String startWork;
    private String stopWork;
    private String order;
    private String status;

    public WeekWork(){

    }

    public WeekWork(int ID, String nameWork, LocalDate startDate, LocalDate endDate, String startWork, String stopWork, String order, String status) {
        this.ID = ID;
        this.nameWork = nameWork;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startWork = startWork;
        this.stopWork = stopWork;
        this.order = order;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameWork() {
        return nameWork;
    }

    public void setNameWork(String nameWork) {
        this.nameWork = nameWork;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getStartWork() {
        return startWork;
    }

    public void setStartWork(String startWork) {
        this.startWork = startWork;
    }

    public String getStopWork() {
        return stopWork;
    }

    public void setStopWork(String stopWork) {
        this.stopWork = stopWork;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
