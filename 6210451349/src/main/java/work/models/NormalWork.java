package work.models;

import java.time.LocalDate;

public class NormalWork {

    private int ID;
    private String nameWork;
    private LocalDate dateWork;
    private String startWork;
    private String endWork;
    private String order;
    private String status;

    public NormalWork(){
    }

    public NormalWork(int ID, String nameWork, LocalDate dateWork, String startWork, String endWork, String order, String status) {
        this.ID = ID;
        this.nameWork = nameWork;
        this.dateWork = dateWork;
        this.startWork = startWork;
        this.endWork = endWork;
        this.order = order;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameWork() {
        return nameWork;
    }

    public void setNameWork(String nameWork) {
        this.nameWork = nameWork;
    }

    public LocalDate getDateWork() {
        return dateWork;
    }

    public void setDateWork(LocalDate dateWork) {
        this.dateWork = dateWork;
    }

    public String getStartWork() {
        return startWork;
    }

    public void setStartWork(String startWork) {
        this.startWork = startWork;
    }

    public String getEndWork() {
        return endWork;
    }

    public void setEndWork(String endWork) {
        this.endWork = endWork;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
