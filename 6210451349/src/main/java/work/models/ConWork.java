package work.models;

public class ConWork {

    private int ID;
    private String nameWork;
    private String namePerson;
    private String order;
    private String status;

    public ConWork() {
    }

    public ConWork(int ID, String nameWork, String namePerson, String order, String status) {
        this.ID = ID;
        this.nameWork = nameWork;
        this.namePerson = namePerson;
        this.order = order;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameWork() {
        return nameWork;
    }

    public void setNameWork(String nameWork) {
        this.nameWork = nameWork;
    }

    public String getNamePerson() {
        return namePerson;
    }

    public void setNamePerson(String namePerson) {
        this.namePerson = namePerson;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
