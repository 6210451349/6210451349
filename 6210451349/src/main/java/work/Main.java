package work;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.github.saacsos.FXRouter;
import work.models.NormalWork;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXRouter.bind(this, primaryStage, "6210451349", 1000, 600);
        configRoute();
        FXRouter.goTo("home");
    }

    private static void configRoute() {
        FXRouter.when("home", "home.fxml");
        FXRouter.when("normal_work", "normal_work.fxml");
        FXRouter.when("normal_work_add", "normal_work_add.fxml");
        FXRouter.when("week_work", "week_work.fxml");
        FXRouter.when("week_work_add", "week_work_add.fxml");
        FXRouter.when("con_work", "con_work.fxml");
        FXRouter.when("con_work_add", "con_work_add.fxml");
        FXRouter.when("project_work", "project_work.fxml");
        FXRouter.when("project_work_add", "project_work_add.fxml");
        FXRouter.when("about", "about.fxml");
    }



    public static void main(String[] args) {
        launch(args);
    }
}
